#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
    for (int i = 0; i < toSort.size(); ++i) {
        for (int y=0; y < toSort.size()-1; ++y) {
            if(toSort[y]>toSort[y+1]){
                    toSort.swap(y,y+1);
            }
        }
    }
}



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}

//for (int i = 0; i < toSort.size(); ++i) {
//    for (int y=1; y < toSort.size(); ++y) {
//        if(toSort[i]>toSort[y]){
//                toSort.swap(i,i+1);
//        }
//    }
//}
