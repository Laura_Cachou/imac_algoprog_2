#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow*w = nullptr;


void selectionSort(Array& toSort){ //toSort est le tableau
	// selectionSort
    //t <= (fleche) tableau de nombre aléatoire
    //Pour chaque indice i de t faire chercher le minimum à partir de i
    //inverser le minimum et la case courante
    //fin Pour
    // swap between the first index and the last

    for (int i = 0; i < toSort.size(); ++i) {
        for (int y = i; y < toSort.size(); ++y) {
            //regarder qui est le minimum
            if(toSort[i]>toSort[y])
                toSort.swap(i,y);
        }
    }
printf("I think it’s sorted");
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
