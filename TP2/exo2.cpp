#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
    Array& sorted=w->newArray(toSort.size());//result=sorted


	// insertion sort from toSort to sorted
	
    sorted[0]= toSort[0];

        for(int n = 1; n<toSort.size(); ++n){ //parce que juste avant on définit la sorted[0]= toSort[0];
            int nouveau=n;
            for(int m=0; m<n; ++m){
                if(sorted[m]>toSort[n]){
                    nouveau=m; //on sait que c'est le plus petit
                    break;//donc on casse la chaine pour recommencer la boucle
                    //car le petit est aller à la fin, construction du tab à l'envers
                }
            }
            sorted.insert(nouveau, toSort[n]);
        }
    printf("I think it’s sorted");
        toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
