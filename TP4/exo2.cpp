#include <time.h>
#include <stdio.h>

#include <QApplication>
#include <QDebug>

#include "tp3.h"
#include "tp4.h"
#include "tp4_exo2.h"
#include "HuffmanNode.h"

_TestMainWindow* w1 = nullptr;
using std::size_t;
using std::string;

void processCharFrequences(string data, Array& frequences);
void buildHuffmanHeap(const Array& frequences, HuffmanHeap& priorityMinHeap, int& heapSize);
HuffmanNode* makeHuffmanSubTree(HuffmanNode* rightNode, HuffmanNode* leftNode);
HuffmanNode* buildHuffmanTree(HuffmanHeap& priorityMinHeap, int heapSize);

string huffmanEncode(const string& toEncode, HuffmanNode* huffmanTree);
string huffmanDecode(const string& toDecode, const HuffmanNode& huffmanTreeRoot);


void main_function(HuffmanNode*& huffmanTree)
{
    string data = "Ouesh, bien ou bien ? Ceci est une chaine de caracteres sans grand interet";

    // this array store each caracter frequences indexed by their ascii code
    Array characterFrequences(256);
    characterFrequences.fill(0);
    // this array store each caracter code indexed by their ascii code
    string characterCodes[256];
    HuffmanHeap priorityMinHeap;
    int heapSize = 0;

    processCharFrequences(data, characterFrequences);
    displayCharacterFrequences(characterFrequences);
    buildHuffmanHeap(characterFrequences, priorityMinHeap, heapSize);
    qDebug() << priorityMinHeap.toString().toStdString().c_str();

    huffmanTree = buildHuffmanTree(priorityMinHeap, heapSize);
    huffmanTree->processCodes("");
    string encoded = huffmanEncode(data, huffmanTree);
    string decoded = huffmanDecode(encoded, *huffmanTree);

    qDebug("Encoded: %s\n", encoded.c_str());
    qDebug("Decoded: %s\n", decoded.c_str());
}


void processCharFrequences(string data, Array& frequences)
{
    /**
      * Fill `frequences` array with each caracter frequence.
      * frequences is an array of 256 int. frequences[i]
      * is the frequence of the caracter with ASCII code i
     **/

    // Your code, compter le nombre de lettre
    frequences.fill(0);
    //data chaine de caractere
    for(int i=0; i<data.size();i++){
        frequences[data[i]]+=1;//comprend directement que c'est ASCII
    }
}

void HuffmanHeap::insertHeapNode(int heapSize, HuffmanNode* newNode)
{
    /**
      * Insert a HuffmanNode into the lower heap. A min-heap put the lowest value
      * as the first cell, so check the parent should be lower than children.
      * Instead of storing int, the cells of HuffmanHeap store HuffmanNode*.
      * To compare these nodes use their frequences.
      * this->get(i): HuffmanNode*  <-> this->get(i)->frequences
      * you can use `this->swap(firstIndex, secondIndex)`
     **/

    // Your code
    int i = heapSize;
    (*this)[i] = newNode;

//    while(i>0 && (*this)[i]>(*this)[(i-1)/2]){
//        swap(i, (i-1)/2);
//        i = (i-1)/2;
//    }

    while(i>0 && this->get(i)->frequences < this->get((i-1)/2)->frequences){//éléménts plus petit qu'il soit le plus haut dans l'abre
        this->swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void buildHuffmanHeap(const Array& frequences, HuffmanHeap& priorityMinHeap, int& heapSize)
{
    /**
      * Do like Heap::buildHeap. Use only non-null frequences
      * Define heapSize as numbers of inserted nodes
      * allocate a HuffmanNode with `new`
     **/

    // Your code
    heapSize = 0;
    for(int i=0; i<frequences.size(); i++){
        if(frequences[i]!=0){
        priorityMinHeap.insertHeapNode(heapSize, new HuffmanNode(i, frequences[i]));
        heapSize+=1;
        }
        //faire heapsize incrémenter pour avancer dans le tableau
        //cree new huffman node
    }
    //structure, tri par ordre de priorité minimum

}

void HuffmanHeap::heapify(int heapSize, int nodeIndex)
{
    /**
      * Repair the heap starting from nodeIndex. this is a min-heap,
      * so check the parent should be lower than children.
      * this->get(i): HuffmanNode*  <-> this->get(i)->frequences
      * you can use `this->swap(firstIndex, secondIndex)`
     **/

    // Your code
    int i = heapSize;
    int i_min = nodeIndex;
    if(nodeIndex*2+1<heapSize && this->get(nodeIndex*2+1)->frequences<this->get(i_min)->frequences){
        i_min=nodeIndex*2+1;
    }
    if(nodeIndex*2+2<heapSize && this->get(nodeIndex*2+2)->frequences<this->get(i_min)->frequences){//cette fois-ci on veut valeur min donc on inverse signe
        i_min=nodeIndex*2+2;
    }
    if(i_min != nodeIndex){
        (*this).swap(nodeIndex, i_min);
        (*this).heapify(i, i_min);
    }

}


HuffmanNode* HuffmanHeap::extractMinNode(int heapSize)
{
    /**
      * Extract the first cell, replace the first cell with the last one and
      * heapify the heap to get a new well-formed heap without the returned cell
      * you can use `this->swap`
     **/

    // Your code
    if(heapSize!=0){
        (*this).swap(0, heapSize-1);
        (*this).heapify(heapSize-1, 0);//échange première valeur avec la dernière valeur
    }
    return this->get(heapSize-1);
}

HuffmanNode* makeHuffmanSubTree(HuffmanNode* rightNode, HuffmanNode* leftNode)
{
    /**
     * Make a subtree (parent + 2 children) with the given 2 nodes.
     * These 2 characters will be the children of a new parent node which character is '\0'
     * and frequence is the sum of the 2 children frequences
     * Return the new HuffmanNode* parent
     **/
    HuffmanNode* parent = new HuffmanNode('\0');
    //somme des enfants
    //parent fleche left
    int somme = 0;
    if(rightNode != nullptr) { //vérifie que le noeud droit n'est pas nul
        somme += rightNode->frequences;
    }
    if(leftNode != nullptr) {
        somme += leftNode->frequences;
    }

    parent->left = leftNode;
    parent->right = rightNode;
    parent->frequences = somme;
    return parent;
}

HuffmanNode* buildHuffmanTree(HuffmanHeap& priorityMinHeap, int heapSize)
{
    /**
      * Build Huffman Tree from the priorityMinHeap, pick nodes from the heap until having
      * one node in the heap. For every 2 min nodes, create a subtree and put the new parent
      * into the heap. The last node of the heap is the HuffmanTree;
      * use extractMinNode()
     **/

    // Your code

    //tant qu'il y a des élements dans le tas (priority), en prendre 2, extraire heap
    //extract min node, avec minsize
    //utiliser makesubstree pour parent
    //insertHeapNode, celui que je viens de créer
    //Heapsize décrémenter et incrémenter (en fonction de si retire ou remet élément avec insertnode)

    while(heapSize>1){
        HuffmanNode* left = priorityMinHeap.extractMinNode(heapSize);
        heapSize-=1;
        HuffmanNode* right = priorityMinHeap.extractMinNode(heapSize);
        heapSize-=1;

        HuffmanNode* parent = makeHuffmanSubTree(right, left);
        priorityMinHeap.insertHeapNode(heapSize, parent);//remet élement dans tas
        heapSize+=1;//incrémente car on retourne un élément dans le tas
    }
    //return new HuffmanNode('?'); //ne retourne plus un nouveau noeud
    return priorityMinHeap.get(0);
}

void HuffmanNode::processCodes(const std::string& baseCode)
{
    /**
      * Travel whole tree of HuffmanNode to determine the code of each
      * leaf/character.
      * Each time you call the left child, add '0' to the baseCode
      * and each time call the right child, add '1'.
      * If the node is a leaf, it takes the baseCode.
     **/
    //si feuille alors mon code=basecode
    //sinon remettre processcode en ajoutant '0' ou '1' selon left ou child
    // Your code
        if (!this->left && !this->right){ //si feuille, pas d'enfant
           this->code=baseCode;
        }
        else{
            if(this->left){
                this->left->processCodes(baseCode+"0");//pour ajouter le nouveau caractère dans la baseCode
            }
            if(this->right){
                this->right->processCodes(baseCode+"1");
            }
        }
}

void HuffmanNode::fillCharactersArray(std::string charactersCodes[])
{
    /**
      * Fill the string array with all nodes codes of the tree
      * It store a node into the cell corresponding to its ascii code
      * For example: the node describing 'O' should be at index 79
     **/
    if (!this->left && !this->right) //si feuille, pas d'enfant
        charactersCodes[this->character] = this->code;
    else {
        if (this->left)
            this->left->fillCharactersArray(charactersCodes);
        if (this->right)
            this->right->fillCharactersArray(charactersCodes);
    }
}

string huffmanEncode(const string& toEncode, HuffmanNode* huffmanTree)
{
    /**
      * Encode a string by using the huffman compression.
      * With the huffmanTree, determine the code for each character
     **/
    //Encoder une chaîne de caractères en utilisant la compression huffman.
    //Avec le huffmanTree, déterminez le code de chaque caractère.

    // Your code
    std::string charactersCodes[256]; // array of 256 huffman codes for each character
    huffmanTree->fillCharactersArray(charactersCodes);
    string encoded = "";
    for(int i=0; i<toEncode.size();i++){
        encoded+=charactersCodes[toEncode[i]];//comprend directement que c'est ASCII, += ppur ne pas écraser la valeur
    }
    //parcourt chaine de caractères, utilise encode
    return encoded;
    return "";
}


string huffmanDecode(const string& toDecode, const HuffmanNode& huffmanTreeRoot)
{
    /**
      * Use each caracters of toDecode, which is '0' either '1',
      * to travel the Huffman tree. Each time you get a leaf, get
      * the decoded character of this node.
     **/
    //Utilisez chaque caractère de toDecode, qui est '0' ou '1',
    //pour parcourir l'arbre de Huffman. Chaque fois que vous atteignez une feuille, vous obtenez
    //le caractère décodé de ce noeud.

    // Your code
    string decoded = "";

    //boucle while
    int i=0;

    const HuffmanNode* cursor; //part de la racine, curseur, const pour pas modifier noeud
    cursor=&huffmanTreeRoot; //redéfinit la racine (repart du haut)
    while(i<toDecode.size()){
        //prendre lettre et rajoute dans decoded
        if(!cursor->left && !cursor->right){
            decoded+=cursor->character; //rajoute la lettre dans la chaine de caractères "decoded"
            cursor=&huffmanTreeRoot; //cursor repart de la racine, en haut du schéma
        }//redéfinit la racine (repart du haut)
        if(toDecode[i]=='0'){
            cursor=cursor->left; //part à gauche
        }if (toDecode[i]=='1'){
            cursor=cursor->right; //part à droite
        }
        i++;
    }
       return decoded;
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Array::wait_for_operations = false;
    w1 = new HuffmanMainWindow(main_function);
    w1->show();
    return a.exec();
}

