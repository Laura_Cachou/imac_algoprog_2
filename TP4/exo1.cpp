#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChildIndex(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChildIndex(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    (*this)[i] = value;

    while(i>0 && (*this)[i]>(*this)[(i-1)/2]){
        swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
//    int i = heapSize;
//    int i_max = nodeIndex;

//    if(i_max!=leftChild(nodeIndex)&& leftChild(nodeIndex)<i){
//        i_max=leftChild(nodeIndex);
//        //swap(i, i_max);
//        swap((*this)[i], (*this)[i_max]);
//        heapify(heapSize, i_max);
//    }

//    if(i_max!=rightChild(nodeIndex) && rightChild(nodeIndex)<i){
//        i_max=rightChild(nodeIndex);
//        //swap(i, i_max);
//        swap((*this)[i], (*this)[i_max]);
//        heapify(heapSize, i_max);
//    }
    int i = heapSize;
    int i_max = nodeIndex;
    if(rightChildIndex(nodeIndex)<i && (*this)[rightChildIndex(nodeIndex)]>(*this)[nodeIndex]){
        i_max=(*this).rightChildIndex(nodeIndex);
    }

    if(leftChildIndex(nodeIndex)<i && (*this)[leftChildIndex(nodeIndex)]>(*this)[i_max]){
        i_max=(*this).leftChildIndex(nodeIndex);
    }

    if(i_max != nodeIndex){
        (*this).swap(nodeIndex, i_max);
        (*this).heapify(i, i_max);
    }
    //tester ensuite avec imax au lieu de nodeIndex
    //si enfant inférieur taille du tableau (heapsize)


    //test entre 1 parents et les enfants, comparé valeur node index avec parents
    //donne une valeur, a comparé avec valeur droite
    //si != de parents, un des enfants pas bon, faire swap
}

//indice heapsize =icomparé taille pour qu'il y ait bien enfant

void Heap::buildHeap(Array& numbers)
{
    //structure, tri apr ordre de priorité minimum
    for(int i=0; i<numbers.size(); i++){
        insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    for(int i=size()-1; i>0; i--){
        swap(0, i);
        heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
