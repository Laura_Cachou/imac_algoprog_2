#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */
    for(int n=0; n<nodeCount; n++){
        GraphNode* new_node = new GraphNode(n);
        this->appendNewNode(new_node);
    }

    for(int i = 0; i < nodeCount; i++){
        for(int j = 0; j < nodeCount ; j++){
            if(adjacencies[i][j]>=1){
            this->nodes[i]->appendNewEdge(this->nodes[j], adjacencies[i][j]);
            }
        }
    }
}


void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
    nodes[nodesSize] = first;
    nodesSize++;
    visited[first->value] = true;
    for(Edge* e = first->edges ; e != nullptr ; e=e->next){
        if(!visited[e->destination->value]){
            deepTravel(e->destination, nodes, nodesSize, visited);
        }
    }

}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */

    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);
    visited[first->value+1] = true;
    int edgeCount=0;

    while(!nodeQueue.empty()){
        GraphNode* current = nodeQueue.front();
        nodes[nodesSize] = current;
        nodesSize++;

        for(int i = 0; i<edgeCount; i++){
            GraphNode* child = current->edges[i].destination;
            if(!visited[child->value]){
                visited[child->value] = true;
                nodeQueue.push(child);
            }
            edgeCount++;
        }
        nodeQueue.pop();
    }
}



bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
      Retourne Vrai si le graphe possède un circuit commençant par first.
	**/
    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);
    visited[first->value+1] = true;

    while(!nodeQueue.empty()){
        GraphNode* current = nodeQueue.front();

        Edge* edge = current->edges;
        while(edge!=nullptr){
            GraphNode* child = edge->destination;
            if(child==first){
                return true;
            }
            if(!visited[child->value]){
                visited[child->value] = false;
                nodeQueue.push(child);
            }
            edge = edge->next;
        }
        nodeQueue.pop();
    }
    return false; //fini de regarder le cycle

    //je pars d'un noeud first, je visite un tableau, visited =0, traverse l'arbre à partir  du noeud
    //faire avec les enfants, tant qu'il y a des enfants, continue visite
    //si dejà visité, ne pas recommencer
    //boucle for sur les enfants
    //rajouter if est ce que c'est mon first

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
