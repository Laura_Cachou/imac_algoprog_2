#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

/**
 * @brief define indexMin and indexMax as the first and the last index of toSearch
 * @param array array of int to process
 * @param toSearch value to find
 * @param indexMin first index of the value to find
 * @param indexMax last index of the value to find
 */
//tabkea
void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax){
	// do not use increments, use two different binary search loop
    //binarySearchAll: rempli l’index minimum et maximum de la valeur toSearch.
    //Si la valeur n’est pas dans le tableau rempli les deux index par −1.
    indexMin = indexMax = -1;
    int start = 0;
    int end = array.size(); //n, taille du tableau
    while (start<end){
        int mid = (start+end)/2;
        if(toSearch > array[mid]){
            start = mid +1;
        }else if(toSearch < array[mid]){
            end = mid;
        }else{
            indexMax = indexMin =mid;

            break;
             //lorsque je suis égale, donc on a trouvé index
        }
     }
    //deux boucles while en décalant gauche droite avec min et max

    while(indexMax+1<array.size()-1 && array[indexMax+1]==array[indexMax]){ //-1 car on décale car tab size -1 = index
        indexMax +=1;
    }

    while(indexMin-1>0 && array[indexMin-1]==array[indexMin]){
        indexMin -=1;
    }
}

int main(int argc, char *argv[]){
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
