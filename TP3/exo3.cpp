#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value){
        this->value = value;
        left=nullptr;
        right=nullptr;
    }

    //insertNumber(int value) : insertNumber modifié pour garder un arbre équilibré.
    //Vous pouvez utiliser le langage que vous souhaitez.
    void insertNumber(int value){
        if(value <this->value){
            if(left==nullptr) {
                left=new SearchTreeNode(value); //pointeur ??
            }else{
                left->insertNumber(value);
            }
        }else{
            if(right==nullptr){
                right=new SearchTreeNode(value);
            }else{
                right->insertNumber(value);
            }
        }
    }

    //height() : retourne la hauteur de l’arbre.
    uint height() const{
        uint left_height = 0;
        uint right_height = 0;
        if(left!=nullptr){
            left_height = left->height();
        }
        if(right!=nullptr){
            right_height = right->height();
        }
        return std::max(left_height, right_height) + 1;
    }


    //nodesCount() : retourne le nombre de noeuds de l’arbre.
    uint nodesCount() const{
        uint count = 1;
        if(left !=nullptr){
            count +=left->nodesCount();
        }
        if(right !=nullptr){
            count +=right->nodesCount();
        }
        return count;
    }

    //isLeaf() : retourne vrai si l’arbre est une feuille, faux sinon.
    bool isLeaf() const{
        return (left==nullptr && right==nullptr);
    }

    //allLeaves(Node* leaves[e, int leavesCount) : rempli le tableau leaves avec toutes les feuilles
    //de l’arbre.
    void allLeaves(Node* leaves[], uint& leavesCount){
        if(isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        }else{
            if(left !=nullptr){
                left->allLeaves(leaves, leavesCount);
            }
            if(right !=nullptr){
                right->allLeaves(leaves, leavesCount);
            }
        }
    }

    //inorderTravel(Node* nodes[e, int nodesCount) : rempli le tableau nodes en parcourant l’arbre
    //dans l’ordre infixe (fils gauche, parent, fils droit).
    void inorderTravel(Node* nodes[], uint& nodesCount){
        if(left !=nullptr){
            left->inorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount]=this;
        nodesCount++;
        if(right !=nullptr){
            right->inorderTravel(nodes, nodesCount);
        }
    }

    //preorderTravel(Node* nodes[e, int nodesCount) : rempli le tableau nodes en parcourant l’arbre
    //dans l’ordre préfixe (parent, fils gauche, fils droit).
    void preorderTravel(Node* nodes[], uint& nodesCount){
        nodes[nodesCount]=this;
        nodesCount++;
        if(left !=nullptr){
            left->preorderTravel(nodes, nodesCount);
        }
        if(right !=nullptr){
            right->preorderTravel(nodes, nodesCount);
        }
    }


    //postorderTravel(Node* nodes[e, int nodesCount) : rempli le tableau nodes en parcourant
    //l’arbre dans l’ordre suffixe (fils gauche, fils droit, parent).
    void postorderTravel(Node* nodes[], uint& nodesCount){
        if(left!=nullptr){
            left->postorderTravel(nodes, nodesCount);
        }
        if(right!=nullptr){
            right->postorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
    }

    Node* find(int value){
        if(this->value ==value){
            return this;
        }
        if(value<this->value && left != nullptr){
            return left->find(value);
        }
        if(value>this->value && right != nullptr){
            return right->find(value);
        }
        return nullptr;
    }


    void reset()
    {
        if(left !=NULL)
            delete left;
        if(right != NULL)
            delete right;
        left=right=NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
