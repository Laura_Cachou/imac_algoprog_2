/*Implémenter une structure DynaTableau et une structure Liste avec les comportements suivants :
- ajoute(int valeur) : Ajoute une nouvelle valeur à la fin de la structure (alloue de la mémoire
en plus si nécessaire)
- recupere(int n) : Retourne le nième entier de la structure
- cherche(int valeur) : Retourne l’index de valeur dans la structure ou -1 s’il n’existe pas
- stocke(int n, int valeur) : Redéfini la nième valeur de la structure avec valeur
La structure DynaTableau représente une structure contenant un tableau qui se réallouera quand la
capacité de celui-ci est depassé.

La structure Liste représente une structure qui aide à retrouver l’ensemble des données à travers
différents noeuds chainés. Ajouter des fonctions à la structure de votre choix pour implémenter le
comportement d’une Pile et d’une File
- pousser_file(int valeur) : Ajoute une valeur à la fin ou au début de la structure
- retirer_file() : Enlève la première valeur ajoutée et la retourne
- pousser_pile(int valeur) : Ajoute une valeur à la fin ou au début de la structure
- retirer_pile() : Enlève la dernière valeur ajoutée et la retourne*/

#include <iostream>
#include <new>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{//liste chainée
    Noeud* premier= nullptr;
    int taille = 0;
};

struct DynaTableau { //vrai tableau, stocke les éléments
    int* donnees = nullptr;//pointeur
    int capacite = 0;
    int taille = 0;
};


//Liste
void initialise(Liste* liste)
{
    liste->premier=nullptr;
    //définir une liste vide, il faut mettre la première cellule point à rien, ptr
    //le pointeur =nullptr

}

bool est_vide(const Liste* liste)
{
     if (liste->premier==nullptr){
         return false;
     }
     return true;
}

void ajoute(Liste* liste, int valeur)
{
    if(liste->premier==nullptr)
    {
        cout<<"Il n'y a aucune valeur"<<endl;
        liste->premier = new Noeud{valeur, nullptr};
        return;
    }else{
        Noeud*anotherNoeud =liste->premier;
        while (anotherNoeud->suivant!=nullptr){
            anotherNoeud=anotherNoeud->suivant;
        }
        anotherNoeud->suivant = new Noeud{valeur, nullptr};
    }
    return;
}

void affiche(const Liste* liste)
{
    cout<<endl<<"Liste : "<<endl;
    Noeud* anotherNoeud=liste->premier;
    while(anotherNoeud!=nullptr) {
        cout<<anotherNoeud->donnee<<endl;
        anotherNoeud = anotherNoeud->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
//    liste->premier=donnee;
//    return;
    Noeud* anotherNoeud=liste->premier;
    int value = 0;
    while(value<n){
        anotherNoeud=anotherNoeud->suivant;
        value++;
    }
    return anotherNoeud->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* anotherNoeud=liste->premier;
    int compteur = 0;
    while(anotherNoeud!=nullptr){
        compteur++;
        if(anotherNoeud->donnee==valeur){
            return compteur;
         }
         anotherNoeud=anotherNoeud->suivant;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    //n = valeur;
    Noeud* anotherNoeud=liste->premier;
    for(int i=0; i<n-1; i++){
        anotherNoeud = anotherNoeud->suivant;
    }
    anotherNoeud->donnee=valeur;
}


//Tableau
void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->taille>=tableau->capacite){
        cout << "On ne peut pas rajouter d'éléments, sinon réallouer" <<endl;
        tableau->capacite = tableau->capacite*2; // évite de faire index=index+1
        int* tab = new int[tableau->capacite];
        for(int i = 0; i < tableau->taille; i++) {
            tab[i] = tableau->donnees[i];
         }
         delete[] tableau->donnees;
         tableau->donnees = tab;
     }
     tableau->donnees[tableau->taille]=valeur; //rajoute une seule valeur
     tableau->taille = tableau->taille+1;
}

void initialise(DynaTableau* tableau, int capacite)//capacite taille max tab
{
    tableau->capacite=capacite;
    tableau->donnees = new int[capacite]; //allouer mémoire du tableau, ce qu'on peut mettre dedans, exemple 20 espaces pour mettres valeurs tab
    tableau->taille=0;
    return;
}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->donnees!=nullptr){
        return false;
    }
    return true;
}

void affiche(const DynaTableau* tableau)
{
    cout <<endl<<"Tableau : "<<endl;
    for(int i = 0; i<tableau->taille; i++){
        cout<<tableau->donnees[i]<<endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    int value = 0;
    for(int i=0; i<tableau->taille; i++){
        if(value==n){
            return tableau->donnees[i];
        }
        value++;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0;i<tableau->taille;i++){
        if(tableau->donnees[i]==valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    int i;
    for(i=0; i<n; i++){
        tableau->donnees[i] = valeur;
    }
}


//Liste
//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    if(liste->premier==nullptr) {
        liste->premier=new Noeud {valeur, nullptr};
        return;
    }

    Noeud* anotherNoeud = liste->premier;
    while(anotherNoeud->suivant != nullptr){
        anotherNoeud = anotherNoeud->suivant;
    }
    anotherNoeud->suivant = new Noeud {valeur, nullptr};
}

int retire_file(Liste* liste)
{
    int last_value = 0; //initialiser
    if (liste != nullptr){
        Noeud* anotherNoeud = liste->premier->suivant;
        last_value = liste->premier->donnee;
        free(liste), liste->premier = nullptr;
        liste->premier = anotherNoeud;
    }
    return last_value;
}


//Tableau
void pousse_pile(DynaTableau* tableau, int valeur)
{
    if(tableau->taille == tableau->capacite) {
        tableau->capacite = tableau->capacite*2; // évite de faire index=index+1
        int* tab = new int[tableau->capacite];
        for(int i = 0; i < tableau->taille; i++) {
            tab[i] = tableau->donnees[i];
        }
        delete[] tableau->donnees;
        tableau->donnees = tab;
    }
    tableau->donnees[tableau->taille] = valeur;
    tableau->taille = tableau->taille + 1;
}

int retire_pile(DynaTableau* tableau)
{
    int last_value = 0;
    if (tableau->taille != 0)
    {
        int taille = tableau->taille;
        delete[] &tableau->donnees[taille];
        tableau->taille = tableau->taille-1;
        return tableau->donnees[taille];
    }
    return last_value;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste 01" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau 01" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }


    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste 02" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau 02" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste a " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau a " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements apres stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    DynaTableau pileTab; // DynaTableau pile;
    DynaTableau fileTab; // DynaTableau file;

    initialise(&pile);
    initialise(&file);
    initialise(&pileTab, 6);
    initialise(&fileTab, 6);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pileTab, i);
    }

    int compteur = 7;
    while(!est_vide(&file) && compteur > 0)
    {
        retire_file(&file);
        affiche(&file);
        compteur--;
    }
    cout << endl;

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 7;
    while(!est_vide(&pileTab) && compteur > 0)
    {
        retire_pile(&pileTab);
        affiche(&pileTab);
        compteur--;
    }
    cout << endl;

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
