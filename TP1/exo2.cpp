#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);

int fibonacci(int value)//value =itération
{
    Context _("fibonacci", value); // do not care about this, it allow the display of call stack
    if(value==0){
        return 0;
    }else if(value==1){
        return 1;
    }else{
    int current = fibonacci(value-1) + fibonacci(value -2); //s'appelle elle-même

    return_and_display(current);
    }
}

//fonction fib(n)
    //si n = 0
//        retourner 0
//    sinon si n = 1
//        retourner 1
//    sinon
//        retourner fib(n - 1) + fib(n - 2)

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new FibonacciWindow(fibonacci); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}
