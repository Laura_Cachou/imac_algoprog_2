#include "tp1.h"
#include <QApplication>
#include <time.h>
#include <math.h>

//J'ai rajouté ça dant le fichier mainwindow.h
//Point square(){
//    float x2=x*x-y*y;
//    float y2=2*x*y;
//    return Point(x2, y2);
//}
//Point operator+(const Point& other)const {
//    return Point(x+other.x, y+other.y);
//}

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    //return 0;

    if(n==0){
    // check n
        return 1;
    }
    if (z.squaredLength()>4) {
    // check length of z
        return n;
    }
    else{ // if Mandelbrot, return 1 or n (check the difference)
        Point new_z = z.square() + point;
        return isMandelbrot(new_z, n-1, point);// otherwise, process the square of z and recall
        // isMandebrot
    }
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



