#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);


int search(int value, Array& toSort, int size)
{
    Context _("search", value, size); // do not care about this, it allow the display of call stack

    // your code
    // check the last cell and if it does not correspond
    // then consider a smaller array when recalling search
    if (size==0){ //si taille tableau est vide, permet aussi de sortir quand on arrive à 0 (sinon continue jusqu'à -1, -2...)
        return -1; //et non return 0 (car ça signifie qu'on a trouvé, alors que nous on a pas trouvé
    }
    else if(toSort[size-1]==value){//faire -1 car tableau décalé par rapport à l'indice //value une valeur spéciale qu va chercher
        //mais d'autres valeurs qui remplient le tableau
        return_and_display(size-1);//retourne index de la valeur trouvée
    }else{ //si on a pas trouvé la variable
        return_and_display(search(value, toSort, size-1));//ce size-1, entrée qui va avoir la prochaine fois, recommence pour un tableau avec taille-1
    }
}

//allEvens(int evens[e, int array[e, int evenSize, int arraySize) : rempli evens avec tous les
//nombres paires de array. evenSize représente le nombre de valeur dans evens (est donc égale
//à 0 au début) et arraySize est le nombre de valeur dans array.

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new SearchWindow(search); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}




